package googlePageTest;

import Google.GooglePage;
import Google.SearchResultsPage;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.FluentWait;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;


public class googlePageTest {


    private WebDriver driver;


    @BeforeClass
    public static void setupClass() {
        ChromeDriverManager.getInstance().setup();
    }


    @Before
    public void setupTest() {
        driver = new ChromeDriver();
    }


    @Test
    public void userCanSearch() {

        GooglePage page = open("http://google.com/ncr", GooglePage.class);
        SearchResultsPage results = page.searchFor("selenide");
        results.getResults().shouldHave(size(10));
        results.getResult(0).shouldHave(text("Selenide: concise UI tests in Java"));
    }


    @After
    public void teardown() {
        if (driver != null) {
            driver.quit();
        }
    }


}
